package org.byteb;

import java.util.function.Function;

public class Utils {
    public static Long timeIt(String message,Runnable func) {
        var timeNow = System.currentTimeMillis();
        func.run();
        var timeTaken = System.currentTimeMillis() - timeNow;
        System.out.println("Time taken for " + message + " = " + timeTaken + "ms");
        return timeTaken;
    }
}
