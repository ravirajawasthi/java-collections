package org.byteb;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;


public class ListRandomAccessTest {

    public void execute() {
        String[] names = { "Arthur", "Kratos", "Dutch", "John", "Atreus" };
        //        List<String> namesList = Arrays.asList(names);
        //        very thin wrapper, does not prevent immutability
        List<String> namesList = List.of(names); // java 9+

        names[ 0 ] = "Joel";

        try {
            namesList.set(2, "Ellie");
        } catch (Exception e) {
            System.out.println("This was expected");
            System.out.println("this shows List.of will make a immutable collection");
        }
        System.out.println("namesList = " + namesList);
        System.out.println("names = " + Arrays.toString(names));

        List<Integer> nums = IntStream.range(1, 1_000_000).boxed().toList();
        for (int i = 0 ; i < 2 ; i++) {
            test(nums, ArrayList::new);
            test(nums, Vector::new);
            test(nums, CopyOnWriteArrayList::new);

            test(nums, java.util.LinkedList::new); //takes lots of time because its O(n) (around 1 second)
        }

    }

    public void test(List<Integer> num, UnaryOperator<List<Integer>> fnc) {
        var temp = fnc.apply(num); long total = 0; var time = System.nanoTime(); try {
            for (int i = 0 ; i < 1000 ; i++) {
                total += temp.get(temp.size() / 2);
            } System.out.println(temp.getClass().getTypeName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } System.out.println("total time taken : " + ( System.nanoTime() - time ) / 1_000_000);
    }

}
