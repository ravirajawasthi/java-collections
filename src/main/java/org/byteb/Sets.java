package org.byteb;


import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Sets {


    static class MutableInteger implements Comparable<MutableInteger> {
        private int value;

        public MutableInteger(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "" + value;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof MutableInteger && this.value == ( (MutableInteger) o ).value;
        }

        @Override
        public int hashCode() {
            return value;
        }

        /**
         * Compares this object with the specified object for order.  Returns a
         * negative integer, zero, or a positive integer as this object is less
         * than, equal to, or greater than the specified object.
         *
         * <p>The implementor must ensure {@link Integer#signum
         * signum}{@code (x.compareTo(y)) == -signum(y.compareTo(x))} for
         * all {@code x} and {@code y}.  (This implies that {@code
         * x.compareTo(y)} must throw an exception if and only if {@code
         * y.compareTo(x)} throws an exception.)
         *
         * <p>The implementor must also ensure that the relation is transitive:
         * {@code (x.compareTo(y) > 0 && y.compareTo(z) > 0)} implies
         * {@code x.compareTo(z) > 0}.
         *
         * <p>Finally, the implementor must ensure that {@code
         * x.compareTo(y)==0} implies that {@code signum(x.compareTo(z))
         * == signum(y.compareTo(z))}, for all {@code z}.
         *
         * @param o the object to be compared.
         * @return a negative integer, zero, or a positive integer as this object
         * is less than, equal to, or greater than the specified object.
         * @throws NullPointerException if the specified object is null
         * @throws ClassCastException   if the specified object's type prevents it
         *                              from being compared to this object.
         * @apiNote It is strongly recommended, but <i>not</i> strictly required that
         * {@code (x.compareTo(y)==0) == (x.equals(y))}.  Generally speaking, any
         * class that implements the {@code Comparable} interface and violates
         * this condition should clearly indicate this fact.  The recommended
         * language is "Note: this class has a natural ordering that is
         * inconsistent with equals."
         */
        @Override
        public int compareTo(MutableInteger that) {
            return Integer.compare(this.value, that.value);
        }
    }

    public static void execute() {
        Set<Integer> pi = Set.of(3, 1, 4, 5, 9);// passing duplicate will result an error
        System.out.println("pi = " + pi); // order of elements will always be different between runs in sets


        /*
            Treeset
         */
        var treeSet = new ConcurrentSkipListSet<>();// Tree set won't work because it's not thread safe


        ThreadLocalRandom.current().ints(1_000_000).parallel().boxed().forEach(num -> treeSet.add(num));
        //This will create a red black tree: max-depth = 40

        System.out.println("treeSet.size() = " + treeSet.size());


        /*
            CopyOnWriteArraySet
         */

        List<Integer> input = IntStream.range(0, 100_000).boxed().toList();
        Utils.timeIt("CopyOnWriteArrayList", () -> new CopyOnWriteArrayList<>(input));
        Utils.timeIt("CopyOnWriteArraySet", () -> new CopyOnWriteArraySet<>(input)); //exponentially slow


        Set<MutableInteger> ints = new TreeSet<>();
        for (int i = 0 ; i < 24 ; i++)
            ints.add(new MutableInteger(i));
        System.out.println(ints);
        System.out.println("ints.contains(17) = " + ints.contains(new MutableInteger(17)));

        for (MutableInteger anInt : ints) {
            //BAD 
            if (anInt.value == 7) anInt.value = 30; //tree set is not aware about this change. hence its no longer
            // sorted. and its inconsistent
        }

        //Adding items to DS should always be immutable

        System.out.println(ints);
        System.out.println("ints.contains(17) = " + ints.contains(new MutableInteger(17)));
    }

}
