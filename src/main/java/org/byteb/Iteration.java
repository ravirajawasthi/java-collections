package org.byteb;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

public class Iteration {
    public void execute(){
        Vector<String> beachToys = new Vector<>();
        /*
        This will give exception with iterator because iterator will quickly be out of sync from the collection.

        Copy on write array list technically works because, everytime new array s
       */
        Collections.addAll(beachToys, "toy1", "toy2", "toy3", "toy4", "toy5", "rage", "razor", "umbrella");

        for (Enumeration<String> iterator = beachToys.elements(); iterator.hasMoreElements();){
            String toy = iterator.nextElement();
            if (toy.startsWith("ra")) beachToys.remove(toy);
        }
        System.out.println(beachToys); // this will not remove razor, because when rage is removed razor takes its
        // place and next is umbrella




        Vector<String> goodBeachToys = new Vector<>();
        Collections.addAll(goodBeachToys, "toy1", "toy2", "toy3", "toy4", "toy5", "rage", "razor", "umbrella");

        for (Iterator<String> iterator = goodBeachToys.iterator() ; iterator.hasNext();){
            String toy = iterator.next();
            if (toy.startsWith("ra")) iterator.remove(); // no arg, because iterator points to an element.
        }
        System.out.println(goodBeachToys); // this will not remove razor, because when rage is removed razor takes its
        // place and next is umbrella



        CopyOnWriteArrayList<String> veryGoodBeachToys = new CopyOnWriteArrayList<>();
        Collections.addAll(veryGoodBeachToys, "toy1", "toy2", "toy3", "toy4", "toy5", "rage", "razor", "umbrella");

        for (Iterator<String> iterator = veryGoodBeachToys.iterator() ; iterator.hasNext();){
            String toy = iterator.next();
            if (toy.startsWith("ra")) veryGoodBeachToys.remove(toy); // no arg, because iterator points to an element.
            if (toy.equals("umbrella")) {
                veryGoodBeachToys.add(0,"rabbit0");
                veryGoodBeachToys.add("rabbit1");
            }
        }
    }
}
