package org.byteb;

import java.security.SecureRandom;
import java.util.*;

public class Sorting {
    record Student(String firstName,String lastName,String major) {
    }

    public void execute() {
        var names = new ArrayList<String>();
        Collections.addAll(names,"Joel","Atreus","Ellie","Kratos","John","Dutch","Morgan","Jack");
        Collections.shuffle(names);
        System.out.println("names = " + names);
        Collections.rotate(names,2);
        System.out.println("names = " + names);
        Collections.reverse(names);
        System.out.println("names = " + names);
        names.sort(null);
        System.out.println("names = " + names);
        names.sort(Comparator.reverseOrder());
        System.out.println("names = " + names);

        List<Student> students = new ArrayList<>();
        Collections.addAll(students,new Student("Eugene","Yadav","Slacking"),new Student("Shimmer","Horse",
                                                                                         "Running"),new Student(
                "Tommy",
                "Hilfiger",
                "Clothes"));


        students.forEach(System.out::println);
        List<Student> newStudents = new ArrayList<>();
        for (int i = 0 ; i < 100 ; i++) {
            for (int j = 0 ; j < 100 ; j++) {
                for (int k = 0 ; k < 100 ; k++) {
                    String firstName = "First" + i;
                    String lastName = "Last" + i;
                    String majorName = "Major" + i;
                    Student student = new Student(firstName,lastName,majorName);
                    newStudents.add(student);
                }

            }
        }
        Collections.shuffle(newStudents,new SecureRandom());
        List<Student> copy = new ArrayList<>(newStudents);
        for (int i = 0 ; i < 5 ; i++) {
            testSortingOldWay(newStudents);
            testSortingNewWay(copy);
        }
    }

    private static void testSortingNewWay(List<Student> copy) {
        long timeNow;

        timeNow = System.currentTimeMillis();
        copy.sort(new Comparator<Student>() {
            /*
            Ugly code, kinda slow
             */
            @Override
            public int compare(Student o1,Student o2) {
                int result = o1.firstName.compareTo(o2.firstName);
                if (result != 0) return result;
                result = o1.lastName.compareTo(o2.lastName);
                if (result != 0) return result;
                return o1.major.compareTo(o2.major);
            }
        });
        System.out.println("Time taken for old comparator = " + ( System.currentTimeMillis() - timeNow ) + "ms");
    }

    private static void testSortingOldWay(List<Student> newStudents) {
        var timeNow = System.currentTimeMillis();
        //Better looking, kinda same speed
        newStudents.sort(Comparator.comparing(Student::firstName,String.CASE_INSENSITIVE_ORDER).
                                 thenComparing(Student::lastName,String.CASE_INSENSITIVE_ORDER).
                                 thenComparing(Student::major,String.CASE_INSENSITIVE_ORDER));
        System.out.println("Time taken for new comparator = " + ( System.currentTimeMillis() - timeNow ) + "ms");
    }


}
