package org.byteb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ArrayListExamples {
    public void execute() {
        var list = new ArrayList<Integer>();
        Collections.addAll(list,3,1,4,1,5,9);
        System.out.println("list = " + list);
        System.out.println("list.contains((1) = " + list.contains(1));
        System.out.println("list.indexOf(1) = " + list.indexOf(1));
        System.out.println("list.contains(8) = " + list.contains(8));
        System.out.println("list.indexOf(8) = " + list.indexOf(8));
        list.sort(null);
        System.out.println("Collections.binarySearch(list, 8) = " + Collections.binarySearch(list,8));
        list.add(~Collections.binarySearch(list,8),8); // will insert element 8 at its right place. list still sorted

        var addList = IntStream.range(0,100_000_000).boxed().collect(Collectors.toList());

        var time = System.nanoTime();
        //Quadratic
        try {
            addList.removeIf(i -> i % 2 == 0);
            //            for (Iterator<Integer> iterator = addList.iterator(); iterator.hasNext();  ){
            //                Integer next = iterator.next();
            //                if (next % 2 == 0) iterator.remove();
            //            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println("Time taken = " + ( System.nanoTime() - time ) / 1_000_000 + "ms");


    }
}
