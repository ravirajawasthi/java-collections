package org.byteb;


public class Main {
    public static void main(String[] args) {


        Pixel[] pixelArray = new Pixel[ 1000 ]; // 12
        // bytes for object, 8 to store array size, 1000
        // * 4 for each pointer to pixel class and 24000
        // for 1000 pixel classes. total is : 28016

        int[] pixelArrayInt = new int[ 1000 ]; //12 bytes
        // for object, 8 to store array size, no need for
        // pointer here because primitive type data. 8 *
        // 1000 for all integers. total is : 8016

        // project valhalla in progress to solve this

        //ListRandomAccess
        //        new ListRandomAccessTest().execute();
        //        System.out.println("=========================================");
        //ArrayList Examples
        //        new ArrayListExamples().execute();
        //Iterators
        //        System.out.println("=========================================");
        //        new Iteration().execute();
        //CopyOnWriteArrayList
        //        System.out.println("=========================================");
        //        new CopyOnWrite().execute();
        //Sorting
        //        System.out.println("=========================================");
        //        new Sorting().execute();
        //        System.out.println("=========================================");
        //        new ParallelSorting().execute();

//        Sets.execute();

        Hashing.execute();

    }

    public class Pixel {  //12 bytes reference size
        // (64bit->12 bytes, 32bit->8 bytes)
        int x; // int takes 4 bytes
        int y;
    }
}

