package org.byteb;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class Hashing {

    record Pixel(int x, int y) implements Comparable<Pixel> {
        //Improving the hashcode will dramatically improve performance.
        //It will minimize collisions very drastically
        //HashMap always increases in size of 2
        //Always implement good hashcode
        //actual hashcode should be small as possible

        @Override
        public int compareTo(Pixel that) {
            return x == that.x ? Integer.compare(y, that.y) : Integer.compare(x, that.x);
        }
    }

    public static void execute() {
        for (int i = 0 ; i < 5 ; i++) {
            testSet(new TreeSet<>());//Look up is log
            testSet(new HashSet<>()); //HashSet is always faster, lookup is constant
        }


    }

    private static void testSet(Set<Integer> set) {
        System.out.println(set.getClass());
        Utils.timeIt("Set adding time", () -> {
            for (int i = 0 ; i < 100_000 ; i++) {
                set.add(i);
            }
        });


        Utils.timeIt("Set getting all time", () -> {
            for (int i = 0 ; i < 10 ; i++) {
                set.containsAll(set);
            }
        });


        Collection<Pixel> addList = new ArrayList<>();
        Collection<Pixel> lookupList = new ArrayList<>();


        //HashSet

        for (int x = 0 ; x < 1920 ; x++) {
            for (int y = 0 ; y < 1080 ; y++) {
                addList.add(new Pixel(
                        x, y
                ));
                lookupList.add(new Pixel(
                        x, y
                ));
            }
        }


        for (int i = 0 ; i < 5 ; i++) {
            AtomicReference<HashSet<Pixel>> newSet = new AtomicReference<>();
            testSet(addList, lookupList, newSet);
            System.gc();
        }


    }

    private static void testSet(Collection<Pixel> addList, Collection<Pixel> lookupList, AtomicReference<HashSet<Pixel>> newSet) {
        Utils.timeIt("Adding time", () -> newSet.set(new HashSet<>(addList)));
        Utils.timeIt("Contains time", () -> newSet.get().containsAll(lookupList));
    }


}
