package org.byteb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class ParallelSorting {

    public void execute() {
        /* BOXING of int increases sorting time by 5X */
        int[] intArray = ThreadLocalRandom.current().ints(1_000_000).toArray();
        Integer[] integerArray = Arrays.stream(intArray).boxed().toArray(Integer[]::new);
        var arrayList = new ArrayList<>();
        Collections.addAll(arrayList,integerArray);

        List<Integer> cowList = new CopyOnWriteArrayList<>(integerArray);

        Utils.timeIt("int sorting",new Runnable() {
            @Override
            public void run() {
                Arrays.parallelSort(intArray);
                //Available for arrays not lists. We can easily convert this to array and then use parallel sort.
            }
        });

        Utils.timeIt("Boxed int sorting",new Runnable() {
            @Override
            public void run() {
                Arrays.parallelSort(integerArray);
            }
        });

        Utils.timeIt("ArrayList sorting",new Runnable() {
            @Override
            public void run() {
                arrayList.sort(null);
            }
        });

        Utils.timeIt("CopyOnWriteArrayList sorting",new Runnable() {
            @Override
            public void run() {
                cowList.sort(null);
                //Cant do parallel sort for CopyOnWriteArrayList
            }
        });

    }


}
