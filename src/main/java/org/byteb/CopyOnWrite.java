package org.byteb;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.IntStream;

public class CopyOnWrite {
    public void execute() {

        testInsertionTime(new CopyOnWriteArrayList<>()); //About 2 seconds
        testInsertionTime(new ArrayList<>()); // about 10 ms

        var badArrayList = new ArrayList<Integer>();

        var timeNow = System.currentTimeMillis();
        IntStream.range(0,50_000).forEach(num -> {
            badArrayList.add(num);
            badArrayList.trimToSize();
        }); //about 6 seconds
        System.out.println("Total time taken for ArrayList and then trimming = " + ( System.currentTimeMillis() - timeNow ) +
                                   "ms");

    }

    private static void testInsertionTime(List<Integer> timeConsumingList) {
        var timeNow = System.currentTimeMillis();
        IntStream.range(0,100_000).forEach(timeConsumingList::add);
        System.out.println("Total time taken for " + timeConsumingList.getClass().getName() + " = " + ( System.currentTimeMillis() - timeNow ) +
                                   "ms");
    }
}
